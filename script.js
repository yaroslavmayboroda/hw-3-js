'use script'

const userInput = prompt('Введіть число:');

if (!isNaN(userInput)) {
    const number = parseInt(userInput);
    const multiplesOfFive = [];

    for (let i = 0; i <= number; i++) {
        if (i % 5 === 0) {
            multiplesOfFive.push(i);
        }
    }


    if (multiplesOfFive.length > 0) {
        console.log('Числа, кратні 5:');
        console.log(multiplesOfFive);
    } else {
        console.log('Sorry, no numbers');
    }
} else {
    console.log('Введено некоректне число');
}